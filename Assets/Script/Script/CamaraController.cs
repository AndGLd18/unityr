﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraController : MonoBehaviour {

    public Transform target;
    public float distance =-5.2f;
    public  float lift =2.15f;

	// Update is called once per frame
	void Update () {
       // transform.position= target.position + new Vector3(0, lift, distance);
        GetComponent<Transform>().position = target.position + new Vector3(0, lift, distance);

        transform.LookAt(target);
	}

}
