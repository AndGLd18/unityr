﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarAnimacion : MonoBehaviour {
    public Animator ani;
    public Animator ani2;
    void Start() {
        ani.enabled=false;
        ani2.enabled = false;
    }

    void OnTriggerEnter() {
        ani.enabled = true;
        ani2.enabled = true;
        Destroy(gameObject);
    }
}
