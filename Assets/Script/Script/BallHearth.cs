﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class BallHearth : MonoBehaviour
{
    public AudioClip GameOver;
    public float maxDistance = -5;
   private bool isReatting=false;
    // Update is called once per frame
    void Update()
    {
        if (transform.position.y <= maxDistance)
        {
            if (isReatting == false) {
               StartCoroutine (RestarLevel());
            }
           

        }
    }

    IEnumerator RestarLevel()
    {
        isReatting = true;
        AudioSource audio = GetComponent<AudioSource>();
        audio.pitch=1;
        audio.clip = GameOver;
        audio.Play();
        yield return new WaitForSeconds(audio.clip.length);
        SceneManager.LoadScene("Level 1");
        
    }
    
}
