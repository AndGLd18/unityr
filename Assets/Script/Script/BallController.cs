﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioClip))]
public class BallController : MonoBehaviour {

    public float speed;
    private Rigidbody rb;
   
    //variable de audio
    public AudioClip hit01;
    public AudioClip hit02;
    public AudioClip hit03;
    //variable del salto
    public float disToGround;
    public float jumpHeight = 8;//altura de salto
    public float jumpSpeed;//velocidad de saldo

    Vector3 velocity;


    void Start() {
        rb = GetComponent<Rigidbody>();
        jumpSpeed = Mathf.Sqrt(-0.5f * Physics.gravity.y * jumpHeight) + 0.1f;
        //obteniendo la distancia desde el centro hasta el suelo
        disToGround = GetComponent<Collider>().bounds.extents.y;
    }
    bool isGrounded() {
        return Physics.Raycast(transform.position,-Vector3.up,disToGround+0.1f);
    }

    // Update is called once per frame
    void FixedUpdate() {
        //Funcion para mover
        float horizontal = Input.GetAxis("Horizontal");
        float Vertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(horizontal, 0.0f, Vertical);
        rb.AddForce(movement * speed);

        //funcion para salta
        velocity = GetComponent<Rigidbody>().velocity;
        if (Input.GetKeyDown("space") && isGrounded()) {


            velocity.y = jumpHeight;
            rb.velocity = velocity;

        }
      
    }


    

    void OnCollisionEnter() {
        StartCoroutine(Audio());
    }



    IEnumerator Audio()
    {
        AudioSource audio = GetComponent<AudioSource>();
        yield return new WaitForSeconds(audio.clip.length);
       int TheHit = Random.Range(0, 3);
       
        if (TheHit == 0)
        {
            audio.clip = hit01;
        }
        else if (TheHit == 1)
        {
            audio.clip = hit02;
        }
        else {
            audio.clip = hit03;
        }
        audio.pitch = Random.Range(0.9f, 1.1f);
        audio.Play();

    }
 
}
