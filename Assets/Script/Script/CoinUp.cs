﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class CoinUp : MonoBehaviour {


    public Transform coinEffect;
    private int CoinValue = 1;
    void OnTriggerEnter(Collider info) {
        if (info.tag == "Player") {

            GameMASTER.CurrentScore+=CoinValue;
           Transform effect=(Transform) Instantiate(coinEffect,transform.position,transform.rotation);
            Destroy(effect.gameObject, 3);
            Destroy(gameObject);
        }

    }

    void Update()
    {
      transform.Rotate(new Vector3(0, 300, 0)* Time.deltaTime );
    }
}
