﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlSubeBaja : MonoBehaviour {
    private Transform miTransform;
    public HingeJoint miHingeJoint;
    private JointSpring miJointSpring;
    private Vector3 eulerDeRotacion;

    private void Awake()
    {
        miTransform = transform;
    }

    private void FixedUpdate()
    {
        eulerDeRotacion = miTransform.localRotation.eulerAngles;

        miJointSpring = miHingeJoint.spring;

        if (eulerDeRotacion.z >= 90 && eulerDeRotacion.z < 270)
            miJointSpring.targetPosition = 180f;
        else
            miJointSpring.targetPosition = 0f;

        miHingeJoint.spring = miJointSpring;
    }
}

