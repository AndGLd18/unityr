﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMASTER : MonoBehaviour {

   public  static int CurrentScore = 0;
    private float offesetY = 40f;
    private float siseX = 90f;
    private float sisezY = 25f;


    public Transform MusicPrefab;
    void Start() {
        CurrentScore = 0;
        if (!GameObject.FindGameObjectWithTag("MM") ){
         Transform mManager=(Transform) Instantiate(MusicPrefab, transform.position, Quaternion.identity);
            mManager.name = MusicPrefab.name;
            DontDestroyOnLoad(mManager);
        }
    }

    void OnGUI() {
        GUI.Box(new Rect( Screen.width/2 - siseX/2, offesetY,   siseX ,sisezY ), "Score"+CurrentScore );
    }
}
